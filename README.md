# Attack Tree Generator [[Online Demo](https://jthuraisamy.gitlab.io/attack-tree)]

## Purpose
    
This YAML template can be used to document attack paths for penetration tests and red team operations.

- **Human-readable**: use your favourite text editor, and version control with a team.
- **Machine-readable**: render the data, build GUIs, and allow programatic analysis.

Node types:

- **Artifacts**: items that can further operation goals (e.g. discovered credentials, hosts, documentation).
- **Capabilities**: desirable abilities that artifacts contribute to (e.g. exploits, production or administrative access).
- **Impacts**: business or technical impacts the operation seeks to achieve with used artifacts and capabilities.

You can describe each node with tags and create a link to form a directional relationship. Tags are customizable and can optionally use Font Awesome icons.

The example included references real-life (and responsibly disclosed) attack paths in the Oracle OPERA hotel management software to access sensitive cardholder and reservation data. Details
[here](http://jackson-t.ca/oracle-opera.html).

## Usage

```
cd client/web/cosmic-conure/src
python3 -m http.server
```